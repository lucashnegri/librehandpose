"""
Código adaptado do repositório https://github.com/spmallick/learnopencv/tree/master/HandPose
Tutorial: https://www.learnopencv.com/hand-keypoint-detection-using-deep-learning-and-opencv/
"""

import argparse
import os
import sys
import cv2
import numpy as np
import config


def carregar_modelo():
    """Carrega o modelo já treinado do arquivo."""
    return cv2.dnn.readNetFromCaffe(config.PROTO_FILE, config.WEIGHTS_FILE)


def processar_imagem(rede, imagem, limiar=0.1):
    """
    Processa uma imagem, retornando até 22 pontos de articulação extraídos da imagem, por mão.

    rede: Modelo carregado
    imagem: Imagem a ser processada
    limiar: Limiar utilizado para a extração dos pontos de articulação
    """
    # preparação da imagem como entrada para a rede
    largura, altura = imagem.shape[1], imagem.shape[0]
    proporcao = largura / altura

    ENT_LARGURA = int(((proporcao * config.ENT_ALTURA) * 8) // 8)
    blob = cv2.dnn.blobFromImage(
        image=imagem,
        scalefactor=1.0 / 255,
        size=(ENT_LARGURA, config.ENT_ALTURA),
        mean=(0, 0, 0),
        swapRB=False,
        crop=False,
    )

    # executa a rede para obter os mapas de probabilidade
    rede.setInput(blob)
    saida = rede.forward()

    # extração dos pontos
    return extrair_pontos(saida, largura, altura, limiar)


def extrair_pontos(saida, largura, altura, limiar):
    """Extrai a lista de até 22 pontos de articulação da saída do modelo (mapas de probabilidade)"""
    # extração dos pontos
    pontos = []

    for i in range(config.N_POINTS):
        probMap = saida[0, i, :, :]
        probMap = cv2.resize(probMap, (largura, altura))
        minVal, prob, minLoc, ponto = cv2.minMaxLoc(probMap)

        if prob > limiar:
            pontos.append((int(ponto[0]), int(ponto[1])))
        else:
            pontos.append(None)

    return pontos


def desenhar_esqueleto(imagem, pontos):
    """Retorna uma imagem com o desenho dos pontos de articulação e do esqueleto."""
    # trabalha com uma cópia
    imagem = np.copy(imagem)

    # desenha o esqueleto
    for A, B in config.POSE_PAIRS:
        if pontos[A] and pontos[B]:
            cv2.line(imagem, pontos[A], pontos[B], color=(0, 255, 255), thickness=2)

            cv2.circle(
                img=imagem,
                center=pontos[A],
                radius=8,
                color=(0, 0, 255),
                thickness=-1,
                lineType=cv2.FILLED,
            )
            cv2.circle(
                img=imagem,
                center=pontos[B],
                radius=4,
                color=(0, 0, 255),
                thickness=-1,
                lineType=cv2.FILLED,
            )

    # identifica os pontos
    for i, p in enumerate(pontos, 1):
        if p:
            cv2.putText(
                img=imagem,
                text="{}".format(i),
                org=(int(p[0]), int(p[1])),
                fontFace=cv2.FONT_HERSHEY_SIMPLEX,
                fontScale=2,
                color=(255, 0, 0),
                thickness=2,
                lineType=cv2.LINE_AA,
            )

    return imagem


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "caminho", help="Caminho para a imagem contendo a mão a ser processada"
    )
    parser.add_argument(
        "-v",
        "--verboso",
        help="Adiciona mais informações na saída",
        action="store_true",
    )
    args = parser.parse_args()

    rede = carregar_modelo()
    imagem = cv2.imread(args.caminho)
    pontos = processar_imagem(rede, imagem)

    # imprime a localização dos pontos
    if args.verboso:
        print(pontos)

    # salva a imagem com os pontos detectados e o esqueleto desenhado
    img = desenhar_esqueleto(imagem, pontos)
    caminho_saida = "saida/" + os.path.basename(args.caminho)
    cv2.imwrite(caminho_saida, img)

