# LibreHandPose

LibreLibras, utilizando OpenPose (modelo de mão).

## Modelo treinado

Baixar o modelo de http://posefs1.perception.cs.cmu.edu/OpenPose/models/hand/pose_iter_102000.caffemodel e colocar na pasta _hand_.

## Dependências

- OpenCV 4
- numpy
